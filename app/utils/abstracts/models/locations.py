from django.db import models

from utils.validators import is_alphanumeric
from utils.validators import is_numeric


class AbstractLocation(models.Model):
    reference = models.CharField(
        max_length=256, validators=[is_alphanumeric], verbose_name="Referencia"
    )
    code = models.CharField(
        max_length=8,
        validators=[is_numeric],
        unique=True,
        verbose_name="Código",
    )

    class Meta:
        abstract = True

    def __str__(self):
        return f"{self.reference} - {self.code}"
