from django.core.validators import RegexValidator


is_alphanumeric = RegexValidator(
    r"^[\w ]*$", message="debe de ser alfanumérico"
)
is_numeric = RegexValidator(r"^[\d]*$", message="debe de ser numérico")
