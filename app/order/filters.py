from django_filters import rest_framework as filters

from order.models import Order


class OrderFilter(filters.FilterSet):
    customer_type = filters.NumberFilter(
        "customer", method="filter_customer_type"
    )
    supplied = filters.BooleanFilter(
        field_name="supplied_at", method="filter_supplied"
    )

    def filter_customer_type(self, queryset, name, value):
        return queryset.filter(customer__customer_type=value)

    def filter_supplied(self, queryset, name, value):
        return queryset.exclude(supplied_at__isnull=value)

    class Meta:
        model = Order
        fields = ["customer_type", "is_urgent", "location_type", "supplied"]
