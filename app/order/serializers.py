from typing import Dict

from rest_framework import serializers

from order.models import ASSOCIATED_COMPANY
from order.models import DISTRIBUTION_CENTER
from order.models import Order
from order.models import OrderItem
from order.models import SUBSIDIARY


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ("item", "quantity")


class OrderSerializer(serializers.ModelSerializer):
    items = OrderItemSerializer(many=True, required=True)
    price = serializers.DecimalField(
        max_digits=12, decimal_places=2, read_only=True
    )

    class Meta:
        model = Order
        fields = (
            "pk",
            "customer",
            "is_urgent",
            "location_type",
            "distribution_center",
            "subsidiary",
            "associated_company",
            "created_at",
            "supplied_at",
            "price",
            "items",
        )
        extra_kwargs = {
            "distribution_center": {"required": False},
            "subsidiary": {"required": False},
            "associated_company": {"required": False},
        }

    def validate(self, data: Dict) -> Dict:
        location_type = data.get("location_type")
        distribution_center = data.get("distribution_center")
        subsidiary = data.get("subsidiary")
        associated_company = data.get("associated_company")

        if location_type == DISTRIBUTION_CENTER:
            if not distribution_center:
                raise serializers.ValidationError(
                    {
                        "distribution_center": "Se necesita el centro de distribución"  # noqa: E501
                    }
                )
            if subsidiary or associated_company:
                raise serializers.ValidationError(
                    {
                        "distribution_center": "Campo inválido, se necesitra el centro de distribución"  # noqa: E501
                    }
                )

        if location_type == SUBSIDIARY:
            if not subsidiary:
                raise serializers.ValidationError(
                    {"subsidiary": "Se necesita la sucursal"}
                )
            if distribution_center or associated_company:
                raise serializers.ValidationError(
                    {"subsidiary": "Campo inválido, se necesitra la sucursal"}
                )

        if location_type == ASSOCIATED_COMPANY:
            if not associated_company:
                raise serializers.ValidationError(
                    {"associated_company": "Se necesita la empresa asociada"}
                )
            if distribution_center or subsidiary:
                raise serializers.ValidationError(
                    {
                        "associated_company": "Campo inválido, se necesitra la empresa asociada"  # noqa: E501
                    }
                )
        return data

    def create(self, validated_data: Dict) -> Order:
        items = validated_data.pop("items")
        order = super(OrderSerializer, self).create(validated_data)

        OrderItem.objects.bulk_create(
            [
                OrderItem(
                    order=order,
                    item=item.get("item"),
                    quantity=item.get("quantity"),
                )
                for item in items
            ]
        )

        return order
