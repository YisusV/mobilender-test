from django.db.models import Count
from django.db.models import DecimalField
from django.db.models import F
from django.db.models import Sum
from django.db.models import Value
from django.db.models.functions import Coalesce
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from customer.models import CustomerType
from order.filters import OrderFilter
from order.models import Order
from order.serializers import OrderSerializer


class OrderView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderSerializer
    filterset_class = OrderFilter

    def get_queryset(self):
        return (
            Order.objects.prefetch_related("items__item")
            .all()
            .annotate(
                price=Coalesce(
                    Sum(
                        F("items__item__price") * F("items__quantity"),
                        output_field=DecimalField(),
                    ),
                    Value(0),
                )
            )
        )


class DashboardView(APIView):
    def get(self, request):
        customer_counter = CustomerType.objects.values(
            "customer_type"
        ).annotate(total=Coalesce(Count("customers"), Value(0)))

        urgent_counter = Order.objects.values("is_urgent").annotate(
            total=Count("is_urgent")
        )
        orders_supplied = Order.objects.filter(
            supplied_at__isnull=False
        ).count()
        orders_not_supplied = Order.objects.filter(
            supplied_at__isnull=True
        ).count()
        location_types = Order.objects.values("location_type").annotate(
            total=Coalesce(Count("location_type"), Value(0))
        )

        payload = {
            "customers": customer_counter,
            "orders": {
                "urgent": urgent_counter,
                "supplied": {
                    "yes": orders_supplied,
                    "no": orders_not_supplied,
                },
                "location_types": location_types,
            },
        }
        return Response(payload)
