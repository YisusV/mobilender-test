from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError

from order.models import ASSOCIATED_COMPANY
from order.models import DISTRIBUTION_CENTER
from order.models import Order
from order.models import OrderItem
from order.models import SUBSIDIARY


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = "__all__"

    def clean(self):
        cleaned_data = super().clean()
        location_type = cleaned_data.get("location_type")
        distribution_center = cleaned_data.get("distribution_center")
        subsidiary = cleaned_data.get("subsidiary")
        associated_company = cleaned_data.get("associated_company")

        if location_type == DISTRIBUTION_CENTER:
            if not distribution_center:
                raise ValidationError("Se necesita el centro de distribución")
            if subsidiary or associated_company:
                raise ValidationError(
                    "Campo inválido, se necesita el centro de distribución"
                )

        if location_type == SUBSIDIARY:
            if not subsidiary:
                raise ValidationError("Se necesita la sucursal")
            if distribution_center or associated_company:
                raise ValidationError(
                    "Campo inválido, se necesita la sucursal"
                )

        if location_type == ASSOCIATED_COMPANY:
            if not associated_company:
                raise ValidationError("Se necesita la empresa asociada")
            if distribution_center or subsidiary:
                raise ValidationError(
                    "Campo inválido, se necesita la empresa asociada"
                )


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    extra = 1


class OrderAdmin(admin.ModelAdmin):
    form = OrderForm
    inlines = (OrderItemInline,)


admin.site.register(Order, OrderAdmin)
