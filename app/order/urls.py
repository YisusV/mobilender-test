from django.urls import path

from order.views import OrderView


app_name = "orders"

urlpatterns = [path("", OrderView.as_view(), name="list")]
