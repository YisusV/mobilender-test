from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from rest_framework.test import APITestCase

from business.models.items import Item
from business.models.locations import AssociatedCompany
from business.models.locations import DistributionCenter
from business.models.locations import Subsidiary
from customer.models import Customer
from order.models import ASSOCIATED_COMPANY
from order.models import DISTRIBUTION_CENTER
from order.models import Order
from order.models import SUBSIDIARY


class TestOrders(APITestCase):
    fixtures = (
        "initial_data/business_locations.json",
        "initial_data/business.json",
        "initial_data/customer_types.json",
        "initial_data/customers.json",
        "initial_data/orders.json",
        "initial_data/users.json",
    )

    @classmethod
    def setUpTestData(cls):
        token = Token.objects.create(user_id=1)
        cls.client_admin = APIClient()
        cls.client_admin.credentials(HTTP_AUTHORIZATION="Token " + token.key)

        cls.client_without_session = APIClient()

        cls.url = reverse("orders:list")

    def setUp(self):
        customer = Customer.objects.only("pk").first().pk

        self.distribution_center = (
            DistributionCenter.objects.only("pk").first().pk
        )
        subsidiary = Subsidiary.objects.only("pk").first().pk
        associated_company = AssociatedCompany.objects.only("pk").first().pk

        items = Item.objects.only("pk").all()

        self.order_request_body = {
            "customer": customer,
            "is_urgent": True,
            "location_type": DISTRIBUTION_CENTER,
            "distribution_center": self.distribution_center,
            "items": [
                {"item": item.pk, "quantity": i}
                for i, item in enumerate(items)
            ],
        }

        self.order_request_body_error_location_type = {
            "customer": customer,
            "is_urgent": True,
            "location_type": DISTRIBUTION_CENTER,
            "subsidiary": subsidiary,
            "items": [
                {"item": item.pk, "quantity": i}
                for i, item in enumerate(items)
            ],
        }

        self.order_request_body_error_too_many_location_types = {
            "customer": customer,
            "is_urgent": True,
            "location_type": DISTRIBUTION_CENTER,
            "distribution_center": self.distribution_center,
            "subsidiary": subsidiary,
            "associated_company": associated_company,
            "items": [
                {"item": item.pk, "quantity": i}
                for i, item in enumerate(items)
            ],
        }

        self.order_request_body_incomplete = {
            "customer": customer,
            "is_urgent": False,
            "location_type": DISTRIBUTION_CENTER,
            "items": [
                {"item": item.pk, "quantity": i}
                for i, item in enumerate(items)
            ],
        }

    def test_unauthorized_request(self):
        response = self.client_without_session.post(
            self.url, self.order_request_body, format="json"
        )
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_method_not_allowed(self):
        response = self.client_admin.put(
            self.url, self.order_request_body, format="json"
        )
        self.assertEqual(
            status.HTTP_405_METHOD_NOT_ALLOWED, response.status_code
        )

        response = self.client_admin.patch(
            self.url, self.order_request_body, format="json"
        )
        self.assertEqual(
            status.HTTP_405_METHOD_NOT_ALLOWED, response.status_code
        )

        response = self.client_admin.delete(self.url)
        self.assertEqual(
            status.HTTP_405_METHOD_NOT_ALLOWED, response.status_code
        )

    def test_create_order_with_invalid_location_type_id(self):
        response = self.client_admin.post(
            self.url,
            self.order_request_body_error_location_type,
            format="json",
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertIn("distribution_center", response.json())

        self.order_request_body_error_location_type.update(
            location_type=SUBSIDIARY,
            distribution_center=self.distribution_center,
        )
        self.order_request_body_error_location_type.pop("subsidiary")
        response = self.client_admin.post(
            self.url,
            self.order_request_body_error_location_type,
            format="json",
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertIn("subsidiary", response.json())

        self.order_request_body_error_location_type.update(
            location_type=ASSOCIATED_COMPANY
        )
        response = self.client_admin.post(
            self.url,
            self.order_request_body_error_location_type,
            format="json",
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertIn("associated_company", response.json())

    def test_create_order_with_too_many_fields(self):
        response = self.client_admin.post(
            self.url,
            self.order_request_body_error_too_many_location_types,
            format="json",
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertIn("distribution_center", response.json())

        self.order_request_body_error_too_many_location_types.update(
            location_type=SUBSIDIARY
        )
        response = self.client_admin.post(
            self.url,
            self.order_request_body_error_too_many_location_types,
            format="json",
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertIn("subsidiary", response.json())

        self.order_request_body_error_too_many_location_types.update(
            location_type=ASSOCIATED_COMPANY
        )
        response = self.client_admin.post(
            self.url,
            self.order_request_body_error_too_many_location_types,
            format="json",
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertIn("associated_company", response.json())

    def test_create_order_with_incomplete_body_request(self):
        response = self.client_admin.post(
            self.url, self.order_request_body_incomplete, format="json"
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertIn("distribution_center", response.json())

    def test_create_order(self):
        response = self.client_admin.post(
            self.url, self.order_request_body, format="json"
        )
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

        json = response.json()

        order = Order.objects.get(pk=json.get("pk"))
        self.assertEqual(json.get("customer"), order.customer.pk)
        self.assertEqual(json.get("is_urgent"), order.is_urgent)
        self.assertEqual(json.get("location_type"), order.location_type)

    def test_list_all_orders(self):
        orders = Order.objects.count()
        response = self.client_admin.get(self.url)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(orders, response.json().get("count"))

    def test_list_urgent_order_to_distribution_center_from_platinum_customer_not_supplied(  # noqa: E501
        self
    ):
        response = self.client_admin.get(
            f"{self.url}?is_urgent=true&location_type=d&customer_type=4&supplied=false"  # noqa: E501
        )

        self.assertEqual(status.HTTP_200_OK, response.status_code)
