from django.db import models

from business.models.items import Item
from business.models.locations import AssociatedCompany
from business.models.locations import DistributionCenter
from business.models.locations import Subsidiary
from customer.models import Customer


ASSOCIATED_COMPANY = "a"
DISTRIBUTION_CENTER = "d"
SUBSIDIARY = "s"


class Order(models.Model):
    LOCATION_TYPES = (
        (DISTRIBUTION_CENTER, "Centro de Distribución"),
        (SUBSIDIARY, "Sucursal"),
        (ASSOCIATED_COMPANY, "Empresa Asociada"),
    )

    customer = models.ForeignKey(
        Customer,
        on_delete=models.PROTECT,
        related_name="orders",
        verbose_name="Cliente",
    )

    is_urgent = models.BooleanField(verbose_name="Es urgente")
    location_type = models.CharField(max_length=1, choices=LOCATION_TYPES)
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de creación"
    )
    supplied_at = models.DateTimeField(
        null=True, blank=True, verbose_name="Fecha de suministro"
    )

    distribution_center = models.ForeignKey(
        DistributionCenter,
        on_delete=models.PROTECT,
        related_name="orders",
        null=True,
        blank=True,
        verbose_name="Centro de distribución",
    )
    subsidiary = models.ForeignKey(
        Subsidiary,
        on_delete=models.PROTECT,
        related_name="orders",
        null=True,
        blank=True,
        verbose_name="Sucursal",
    )
    associated_company = models.ForeignKey(
        AssociatedCompany,
        on_delete=models.PROTECT,
        related_name="orders",
        null=True,
        blank=True,
        verbose_name="Empresa asociada",
    )

    class Meta:
        verbose_name = "pedido"
        constraints = [
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_location_type_match",
                check=(
                    models.Q(
                        location_type=ASSOCIATED_COMPANY,
                        associated_company__isnull=False,
                        distribution_center__isnull=True,
                        subsidiary__isnull=True,
                    )
                    | models.Q(
                        location_type=DISTRIBUTION_CENTER,
                        associated_company__isnull=True,
                        distribution_center__isnull=False,
                        subsidiary__isnull=True,
                    )
                    | models.Q(
                        location_type=SUBSIDIARY,
                        associated_company__isnull=True,
                        distribution_center__isnull=True,
                        subsidiary__isnull=False,
                    )
                ),
            )
        ]


class OrderItem(models.Model):
    order = models.ForeignKey(
        Order,
        on_delete=models.PROTECT,
        related_name="items",
        verbose_name="Pedido",
    )
    item = models.ForeignKey(
        Item,
        on_delete=models.PROTECT,
        related_name="orders",
        verbose_name="Artículo",
    )
    quantity = models.IntegerField(default=1, verbose_name="Cantidad")

    class Meta:
        verbose_name = "detalle de pedido"
