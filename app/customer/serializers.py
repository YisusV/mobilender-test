from rest_framework import serializers

from customer.models import Customer


class CustomerSerializer(serializers.ModelSerializer):
    customer_type = serializers.StringRelatedField()

    class Meta:
        model = Customer
        fields = ("pk", "name", "code", "customer_type")
