from django.urls import path

from customer.views import CustomerView


app_name = "customers"

urlpatterns = [path("", CustomerView.as_view(), name="list")]
