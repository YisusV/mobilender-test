from django.core.validators import MinLengthValidator
from django.db import models

from utils.abstracts.models.dates import AbstractDates
from utils.validators import is_alphanumeric


class CustomerType(models.Model):
    customer_type = models.CharField(
        max_length=32,
        validators=[is_alphanumeric],
        verbose_name="Tipo de cliente",
    )

    class Meta:
        verbose_name = "tipo de cliente"

    def __str__(self):
        return self.customer_type


class Customer(AbstractDates):
    name = models.CharField(
        max_length=256,
        validators=[is_alphanumeric, MinLengthValidator(2)],
        verbose_name="Nombre",
    )
    code = models.CharField(
        max_length=32, validators=[is_alphanumeric], verbose_name="Código"
    )
    address = models.TextField(verbose_name="Dirección")
    photo = models.ImageField(upload_to="customers", null=True)
    customer_type = models.ForeignKey(
        CustomerType,
        on_delete=models.PROTECT,
        related_name="customers",
        verbose_name="Tipo de Cliente",
    )

    class Meta:
        verbose_name = "cliente"

    def __str__(self):
        return self.name
