from django.contrib import admin

from customer.models import Customer
from customer.models import CustomerType


class CustomerAdmin(admin.ModelAdmin):
    list_display = ("name", "code", "customer_type", "address")
    list_filter = ("customer_type", "created_at")
    readonly_fields = ("created_at",)
    search_fields = ("name", "code", "address")


admin.site.register(Customer, CustomerAdmin)
admin.site.register(CustomerType)
