from django_filters import rest_framework as filters
from rest_framework import filters as search_filter
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from customer.models import Customer
from customer.serializers import CustomerSerializer


class CustomerView(ListAPIView):
    queryset = (
        Customer.objects.select_related("customer_type")
        .all()
        .only("pk", "name", "code", "customer_type")
    )
    permission_classes = (IsAuthenticated,)
    serializer_class = CustomerSerializer
    filter_backends = (filters.DjangoFilterBackend, search_filter.SearchFilter)
    search_fields = ("name", "code")
