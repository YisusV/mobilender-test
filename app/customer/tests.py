from django.db.models import Q
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from rest_framework.test import APITestCase

from customer.models import Customer


class TestCustomers(APITestCase):
    fixtures = (
        "initial_data/customer_types.json",
        "initial_data/customers.json",
        "initial_data/users.json",
    )

    @classmethod
    def setUpTestData(cls):
        token = Token.objects.create(user_id=1)
        cls.client_logged = APIClient()
        cls.client_logged.credentials(HTTP_AUTHORIZATION="Token " + token.key)

        cls.client_without_session = APIClient()

        cls.url = reverse("customers:list")

    def test_unauthorized_request(self):
        response = self.client_without_session.get(self.url)
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_list_customers(self):
        customer_count = Customer.objects.count()

        response = self.client_logged.get(self.url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        json = response.json()

        self.assertEqual(json.get("count"), customer_count)

    def test_list_customers_with_search(self):
        customer_count = Customer.objects.filter(
            Q(name__icontains="patio") | Q(code__icontains="patio")
        ).count()

        response = self.client_logged.get(f"{self.url}?search=patio")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        json = response.json()

        self.assertEqual(json.get("count"), customer_count)

    def test_list_customers_with_search_empty_results(self):
        customer_count = Customer.objects.filter(
            Q(name__icontains="qwe") | Q(code__icontains="qwe")
        ).count()

        response = self.client_logged.get(f"{self.url}?search=qwe")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        json = response.json()

        self.assertEqual(json.get("count"), customer_count)
