from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from rest_framework.test import APITestCase


class TestUserSignIn(APITestCase):
    fixtures = ("initial_data/users.json",)

    @classmethod
    def setUpTestData(cls):
        cls.client = APIClient()
        cls.url = reverse("users:sign-in")

    def setUp(self):
        self.user_credentials = {
            "email": "prueba@mobilender.test",
            "password": "12345mobil",
        }

        self.user_invalid_credentials = {
            "email": "invalid@email.com",
            "password": "wrong_password",
        }

    def test_sign_in_user(self):
        response = self.client.post(
            self.url, self.user_credentials, format="json"
        )
        json = response.json()

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        token = Token.objects.get(user__username="prueba@mobilender.test")
        self.assertIn("token", json)
        self.assertEqual(token.key, json.get("token"))

    def test_sign_in_mismatch_user_credentials(self):
        response = self.client.post(
            self.url, self.user_invalid_credentials, format="json"
        )

        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_sign_in_bad_request(self):
        response = self.client.post(self.url, {}, format="json")

        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
