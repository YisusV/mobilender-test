from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.response import Response
from rest_framework.views import APIView

from users.serializers import SignInSerializer


class SignInView(APIView):
    serializer_class = SignInSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        username = serializer.validated_data.get("email").lower()
        password = serializer.validated_data.get("password")

        user = authenticate(username=username, password=password)
        if not user:
            raise AuthenticationFailed("Usuario o contraseña no válidos")

        if hasattr(user, "auth_token"):
            user.auth_token.delete()
        token = Token.objects.create(user=user)

        payload = {"token": token.key, "user": user.get_full_name()}

        return Response(payload)
