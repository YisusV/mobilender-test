from typing import Dict

from rest_framework import serializers


def field_required(field_name: str) -> str:
    return f'Por favor, ingresar "{field_name}".'


def field_exceeds_maximum_allowed(field_name: str, maximum: int) -> str:
    return f'El campo "{field_name}" excede el máximo permitido de {maximum} caracteres.'  # noqa: E501


def field_does_not_meet_minimum_allowed(field_name: str, minimum: int) -> str:
    return f'El campo "{field_name}" no cumple el mínimo permitido de {minimum} caracteres.'  # noqa: E501


def field_does_not_meet_requirements(field_name: str) -> str:
    return f'El campo "{field_name}" no cumple con los requisitos.'


def field_with_length_validations(
    max_length: int, min_length: int, field: str
) -> Dict:
    return {
        "error_messages": {
            "blank": field_required(field),
            "invalid": field_does_not_meet_requirements(field),
            "max_length": field_exceeds_maximum_allowed(field, max_length),
            "min_length": field_does_not_meet_minimum_allowed(
                field, min_length
            ),
            "required": field_required(field),
        },
        "max_length": max_length,
        "min_length": min_length,
        "write_only": True,
    }


class SignInSerializer(serializers.Serializer):
    email = serializers.EmailField(
        **field_with_length_validations(128, 6, "email")
    )
    password = serializers.CharField(
        **field_with_length_validations(64, 8, "contraseña")
    )
