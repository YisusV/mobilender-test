from rest_framework import serializers

from business.models.items import Item


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ("pk", "code", "description", "price")
