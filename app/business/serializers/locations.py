from rest_framework import serializers

from business.models.locations import AssociatedCompany
from business.models.locations import DistributionCenter
from business.models.locations import Subsidiary


class DistributionCenterSerializer(serializers.ModelSerializer):
    class Meta:
        model = DistributionCenter
        fields = "__all__"


class SubsidiarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Subsidiary
        fields = "__all__"


class AssociatedCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = AssociatedCompany
        fields = "__all__"
