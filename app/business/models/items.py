from django.db import models

from business.models.suppliers import Supplier
from utils.abstracts.models.dates import AbstractDates
from utils.validators import is_numeric


class Item(AbstractDates):
    code = models.CharField(
        max_length=8,
        validators=[is_numeric],
        unique=True,
        verbose_name="Código",
    )
    description = models.TextField(verbose_name="Descripción")
    price = models.DecimalField(
        max_digits=12, decimal_places=2, default=0, verbose_name="Precio"
    )

    class Meta:
        verbose_name = "artículo"

    def __str__(self):
        return self.code


class ItemSupplier(AbstractDates):
    item = models.ForeignKey(
        Item,
        on_delete=models.PROTECT,
        related_name="suppliers",
        verbose_name="Articulo",
    )
    supplier = models.ForeignKey(
        Supplier,
        on_delete=models.PROTECT,
        related_name="items",
        verbose_name="Proveedor",
    )
    price = models.DecimalField(
        max_digits=12, decimal_places=2, verbose_name="Precio"
    )

    class Meta:
        unique_together = ("item", "supplier")
        verbose_name = "artículo por proveedor"
        verbose_name_plural = "artículos por proveedor"

    def __str__(self):
        return f"{self.item} - {self.supplier}"
