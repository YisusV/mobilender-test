from django.db import models

from utils.abstracts.models.dates import AbstractDates
from utils.validators import is_alphanumeric


class Supplier(AbstractDates):
    name = models.CharField(
        max_length=256,
        validators=[is_alphanumeric],
        unique=True,
        verbose_name="Nombre",
    )
    address = models.TextField(verbose_name="Dirección")

    class Meta:
        verbose_name = "proveedor"
        verbose_name_plural = "Proveedores"

    def __str__(self):
        return self.name
