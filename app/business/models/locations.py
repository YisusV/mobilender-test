from django.db import models

from utils.abstracts.models.locations import AbstractLocation
from utils.validators import is_alphanumeric


class DistributionCenter(models.Model):
    warehouse = models.CharField(
        max_length=256,
        validators=[is_alphanumeric],
        unique=True,
        verbose_name="Almacén",
    )

    class Meta:
        verbose_name = "centro de distribución"
        verbose_name_plural = "centros de distribución"

    def __str__(self):
        return self.warehouse


class Subsidiary(AbstractLocation):
    class Meta:
        verbose_name = "sucursal"
        verbose_name_plural = "sucursales"


class AssociatedCompany(AbstractLocation):
    class Meta:
        verbose_name = "empresa asociada"
        verbose_name_plural = "empresas asociadas"
