from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from rest_framework.test import APITestCase

from business.models.locations import DistributionCenter


class TestDistributionCenter(APITestCase):
    fixtures = (
        "initial_data/business_locations.json",
        "initial_data/users.json",
    )

    @classmethod
    def setUpTestData(cls):
        token = Token.objects.create(user_id=1)
        cls.client_logged = APIClient()
        cls.client_logged.credentials(HTTP_AUTHORIZATION="Token " + token.key)

        cls.client_without_session = APIClient()

        cls.url = reverse("businesses:dc-list")

    def test_unauthorized_request(self):
        response = self.client_without_session.get(self.url)
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_list_distribution_centers(self):
        distribution_center_count = DistributionCenter.objects.count()

        response = self.client_logged.get(self.url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        json = response.json()

        self.assertEqual(json.get("count"), distribution_center_count)

    def test_list_distribution_centers_with_search(self):
        distribution_center_count = DistributionCenter.objects.filter(
            warehouse__icontains="Mont"
        ).count()

        response = self.client_logged.get(f"{self.url}?search=Mont")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        json = response.json()

        self.assertEqual(json.get("count"), distribution_center_count)

    def test_list_distribution_centers_with_search_empty_results(self):
        distribution_center_count = DistributionCenter.objects.filter(
            warehouse__icontains="qwe"
        ).count()

        response = self.client_logged.get(f"{self.url}?search=qwe")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        json = response.json()

        self.assertEqual(json.get("count"), distribution_center_count)
