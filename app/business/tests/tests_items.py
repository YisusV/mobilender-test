from django.db.models import Q
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from rest_framework.test import APITestCase

from business.models.items import Item


class TestItems(APITestCase):
    fixtures = ("initial_data/business.json", "initial_data/users.json")

    @classmethod
    def setUpTestData(cls):
        token = Token.objects.create(user_id=1)
        cls.client_logged = APIClient()
        cls.client_logged.credentials(HTTP_AUTHORIZATION="Token " + token.key)

        cls.client_without_session = APIClient()

        cls.url = reverse("businesses:items-list")

    def test_unauthorized_request(self):
        response = self.client_without_session.get(self.url)
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_list_items(self):
        item_count = Item.objects.count()

        response = self.client_logged.get(self.url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        json = response.json()

        self.assertEqual(json.get("count"), item_count)

    def test_list_items_with_search(self):
        item_count = Item.objects.filter(
            Q(code__icontains="lap") | Q(description__icontains="lap")
        ).count()

        response = self.client_logged.get(f"{self.url}?search=lap")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        json = response.json()

        self.assertEqual(json.get("count"), item_count)

    def test_list_items_with_filter_supplier(self):
        item_count = Item.objects.filter(suppliers__supplier=2).count()

        response = self.client_logged.get(f"{self.url}?suppliers__supplier=2")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        json = response.json()

        self.assertEqual(json.get("count"), item_count)

    def test_list_items_with_search_empty_results(self):
        item_count = Item.objects.filter(
            Q(code__icontains="qwe") | Q(description__icontains="qwe")
        ).count()

        response = self.client_logged.get(f"{self.url}?search=qwe")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        json = response.json()

        self.assertEqual(json.get("count"), item_count)
