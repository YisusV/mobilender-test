from django_filters import rest_framework as filters
from rest_framework import filters as search_filter
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from business.models.items import Item
from business.serializers.items import ItemSerializer


class ItemView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    filter_backends = (filters.DjangoFilterBackend, search_filter.SearchFilter)
    search_fields = ("code", "description")
    filterset_fields = ("suppliers__supplier",)
