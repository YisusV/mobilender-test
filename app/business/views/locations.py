from django_filters import rest_framework as filters
from rest_framework import filters as search_filter
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from business.models.locations import AssociatedCompany
from business.models.locations import DistributionCenter
from business.models.locations import Subsidiary
from business.serializers.locations import AssociatedCompanySerializer
from business.serializers.locations import DistributionCenterSerializer
from business.serializers.locations import SubsidiarySerializer


class DistributionCenterView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = DistributionCenter.objects.all()
    serializer_class = DistributionCenterSerializer
    filter_backends = (filters.DjangoFilterBackend, search_filter.SearchFilter)
    search_fields = ("warehouse",)


class SubsidiaryView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Subsidiary.objects.all()
    serializer_class = SubsidiarySerializer
    filter_backends = (filters.DjangoFilterBackend, search_filter.SearchFilter)
    search_fields = ("reference", "code")


class AssociatedCompanyView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AssociatedCompany.objects.all()
    serializer_class = AssociatedCompanySerializer
    filter_backends = (filters.DjangoFilterBackend, search_filter.SearchFilter)
    search_fields = ("reference", "code")
