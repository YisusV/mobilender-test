from django.contrib import admin

from business.models.items import Item
from business.models.items import ItemSupplier
from business.models.locations import AssociatedCompany
from business.models.locations import DistributionCenter
from business.models.locations import Subsidiary
from business.models.suppliers import Supplier


class ItemSupplierInline(admin.TabularInline):
    model = ItemSupplier
    extra = 0


class ItemAdmin(admin.ModelAdmin):
    list_display = ("code", "description")
    list_filter = ("created_at", "updated_at")
    search_fields = ("code", "description")

    inlines = (ItemSupplierInline,)


class SupplierAdmin(admin.ModelAdmin):
    list_display = ("name", "address")
    list_filter = ("created_at", "updated_at")
    search_fields = ("name", "address")

    inlines = (ItemSupplierInline,)


class AssociatedCompanySubsidiaryAdmin(admin.ModelAdmin):
    list_display = ("reference", "code")
    search_fields = ("reference", "code")


admin.site.register(Item, ItemAdmin)
admin.site.register(DistributionCenter)
admin.site.register(Subsidiary, AssociatedCompanySubsidiaryAdmin)
admin.site.register(AssociatedCompany, AssociatedCompanySubsidiaryAdmin)
admin.site.register(Supplier, SupplierAdmin)
