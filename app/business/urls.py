from django.urls import path

from business.views.items import ItemView
from business.views.locations import AssociatedCompanyView
from business.views.locations import DistributionCenterView
from business.views.locations import SubsidiaryView

app_name = "businesses"

urlpatterns = [
    path(
        "associated-companies/",
        AssociatedCompanyView.as_view(),
        name="ac-list",
    ),
    path(
        "distribution-centers/",
        DistributionCenterView.as_view(),
        name="dc-list",
    ),
    path("items/", ItemView.as_view(), name="items-list"),
    path("subsidiaries/", SubsidiaryView.as_view(), name="subsidiary-list"),
]
