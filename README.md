# Mobilender backend test

[![Python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
[![Black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Proyecto de prueba técnica para Mobilender.

Se desarrollaron algunos servicios extra para poder simular la obtención de la información para crear una orden,
así como un inicio de sesión para bloquear los servicios a personal autorizado.

Accesos administrador Django
```sh
usuario: root
contraseña: 12345678mobilender
```

## Instalación

Asegurar tener instalados [Python 3.7+](https://www.python.org/downloads) y [Docker](https://www.docker-com/).

```sh
make setup
```

Esto con la finalidad de que algunos IDEs puedan reconocer la carpeta como el ambiente de desarrollo.

Se generará un archivo `.env` el cual contendrá las variables de entorno a utilizar, por lo que es necesario modificar con los valores correspondientes antes de iniciar el stack. Por _default_ vienen asignados los valores para inicar el proyecto en local.

## Ejecución

Una vez modificado el archivo `.env` con los valores requeridos ya se pueden iniciar los contenedores del proyecto con el siguiente comando:

```sh
make start
```

La primera vez que se levanta el stack con docker-compose tomará unos minutos en lo que se construye la imagen principal definida con el archivo `compose/Dockerfile`.

### Contenedores

Para detener la ejecución de los contenedores del proyecto está el comando:

```sh
make stop
```

Para correr las migraciones del proyecto se tiene el comando:

```sh
make migrate
```

El cual buscará modificaciones de los modelos del proyecto y aplicará los cambios sobre la base de datos.

En caso de requerir acceder al __shell__ del contenedor está el comando:

```sh
make bash
```

Para ver los logs de los contenedores:

```sh
make logs
```

## Limpieza del ambiente

Para limpiar el ambiente solo necesitas corre el comando:

```sh
make clean
```

El cual detendrá la ejecución de los contenedores y limpiará los archivos y carpetas de _cache_ generados por `python`.
