SHELL=/bin/sh
PATH := .venv/bin:$(PATH)
WSL_DOCKER := $(shell which docker.exe)


ifdef WSL_DOCKER
DOCKER := docker.exe
DOCKER_COMPOSE := docker-compose.exe
else
DOCKER := docker
DOCKER_COMPOSE := docker-compose
endif

# Clean
clean:
	@echo "Cleaning python cache";
	@find . -type f -name "*.py[co]" -delete;
	@find . -type d -name "__pycache__" -delete;
	@echo "Cleaning containers";
	@make stop all=true;

nuke: clean
	@rm -rf .venv
	@$(DOCKER) rmi $$($(DOCKER) images -q mobilender/api)

# Environment
install:
	@( \
		if [ ! -d .venv ] ; then python3 -m venv --copies .venv; fi; \
		source .venv/bin/activate; \
		pip install -qU pip; \
		pip install -r requirements.txt; \
		pip install pre-commit; \
		pre-commit install; \
	);

set-env:
	@if [ ! -f .env ] ; then cp env.example .env ; fi

setup: set-env install

start:
	@echo "Starting project";
	@( \
		if [[ "$(force)" == "true" ]]; then \
			$(DOCKER_COMPOSE) -f docker-compose.yml up -d --build; \
		else \
			$(DOCKER_COMPOSE) -f docker-compose.yml up -d; \
		fi; \
	)
	@$(DOCKER_COMPOSE) -f docker-compose.yml run --rm api python manage.py loaddata initial_data/business.json initial_data/business_locations.json initial_data/customers.json initial_data/customer_types.json initial_data/orders.json initial_data/users.json

	@make logs

stop:
	@echo "Stoping containers";
	@$(DOCKER_COMPOSE) -f docker-compose.yml down;
	@( \
		if [[ "$(all)" == "true" ]]; then \
			$(DOCKER_COMPOSE) -f docker-compose.yml down -v; \
		fi; \
	)

logs:
	@$(DOCKER_COMPOSE) -f docker-compose.yml logs -f

sh:
	@$(DOCKER_COMPOSE) -f docker-compose.yml exec api sh

migrate:
	@echo "Looking for changes"
	@$(DOCKER_COMPOSE) -f docker-compose.yml run --rm api python manage.py makemigrations $(model);
	@echo "Applying changes"
	@$(DOCKER_COMPOSE) -f docker-compose.yml run --rm api python manage.py migrate;

tests:
	@echo "Run tests"
	@$(DOCKER_COMPOSE) -f docker-compose.yml run --rm api python manage.py test --parallel -v 2

coverage:
	@echo "Run coverage"
	@$(DOCKER_COMPOSE) -f docker-compose.yml run --rm api coverage run --source='.' manage.py test -v 2
	@$(DOCKER_COMPOSE) -f docker-compose.yml run --rm api coverage report
	@$(DOCKER_COMPOSE) -f docker-compose.yml run --rm api coverage html
